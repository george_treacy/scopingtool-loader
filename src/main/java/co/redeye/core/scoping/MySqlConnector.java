package co.redeye.core.scoping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by george on 25/09/15.
 */
public class MySqlConnector {

    private static final Logger logger = LoggerFactory.getLogger(MySqlConnector.class);

    private static final String localConnectionString = "jdbc:mysql://localhost/redeye?user=root&password=";
    private static final String remoteConnectionString = "jdbc:mysql://george-rds.cjosmri8vcnm.ap-southeast-2.rds.amazonaws.com/redeye?user=george&password=today123";

    private static Connection conn;

    public static Connection getConnection(final Boolean local) {
        if (conn == null) {
            final String connectionString = local ? localConnectionString : remoteConnectionString;
            createConnection(connectionString);
        }
        return conn;
    }

    private static void createConnection(final String connectionString) {
        try {
            conn = DriverManager.getConnection(connectionString);
        } catch (SQLException ex) {
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            throw new RuntimeException("MySQL connection failed.");
        }
    }

}
