package co.redeye.core.scoping;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * Created by george on 25/09/15.
 *
 * This is a CLI utility that takes a local absolute path
 * and walks the directory structure, returning the path
 * to every file and then sha256 it and jdbc it into a local
 * mysql db into the redeye.file_header table.
 *
 * > desc  redeye.file_header
 *
 */
public class LocalLoader {

    //private static final String filePath = "/media/george/Seagate Backup Plus Drive/QUU First Set 150915";
    // private static final String filePath = "/run/media/george/Seagate Backup Plus Drive";
    //private static final String filePath = "/home/george/dev/alacer-docs";

    private static final String batchId = UUID.randomUUID().toString();

    private static Boolean localFlag = false;

    static void usage() {
        System.err.println("java LocalLoader <path> [-l local-db]");
        System.exit(-1);
    }

    public static void main(String[] args) throws IOException {
        Path startingDir = null;

        if (args.length == 1) {
            startingDir = Paths.get(args[0]);
        } else if (args.length == 2) {
            startingDir = Paths.get(args[0]);
            if ("-l".equals(args[1])) {
                localFlag = true;
            } else {
                usage();
            }
        } else {
            usage();
        }

        System.out.print("Batch ID: " + batchId + " ");
        System.out.println("Starting Directory: " + startingDir);

        FileStalker stalker = new FileStalker(batchId, localFlag);
        Files.walkFileTree(startingDir, stalker);
        stalker.done(startingDir.toAbsolutePath().toString());
    }
}
