package co.redeye.core.scoping;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.parser.DocumentScrape;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import org.apache.tika.metadata.Metadata;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Created by george on 23/09/15.
 */
public class FileStalker extends SimpleFileVisitor<Path> {

    private static final Logger logger = LoggerFactory.getLogger(FileStalker.class);

    private FileProcessor fileProcessor;
    private String batchId = null;

    FileStalker(final String batchId, final Boolean localFlag) {
        this.batchId = batchId;
        fileProcessor = new FileProcessor(localFlag);
    }

    void getNextFile(Path file) {
        Path name = file.getFileName();
        fileProcessor.process(batchId, file);
    }

    void done(final String startDirectory) {
        fileProcessor.cleanUpConnection();
        logger.debug("Done walking directory:  " + startDirectory);
        logger.debug("Batch Id:  " + batchId);
    }

    void printItem(final Map<String, AttributeValue> item) {
        for (String key : item.keySet()) {
            if (!"bodyText".equals(key))
                logger.debug(key + " " + item.get(key));
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        getNextFile(file);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println(exc);
        return CONTINUE;
    }
}