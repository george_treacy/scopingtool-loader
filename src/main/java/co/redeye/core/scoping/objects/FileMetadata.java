package co.redeye.core.scoping.objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.File;

/**
 * Created by george on 25/09/15.
 */
public class FileMetadata {

    private static final Integer MAX_VCHAR_LENGTH = 256;

    private Integer fileId;
    private Integer keyId;
    private String value;

    public FileMetadata(final Integer fileId, final Integer keyId, final String value) {
        this.fileId = fileId;
        this.keyId = keyId;
        this.value = value;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public Integer getKeyId() {
        return keyId;
    }

    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        if (value != null && value.length() > MAX_VCHAR_LENGTH)
            this.value = value.substring(0, MAX_VCHAR_LENGTH - 1);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
