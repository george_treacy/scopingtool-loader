package co.redeye.core.scoping.objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.math.BigInteger;

/**
 * Created by george on 25/09/15.
 */
public class FileHeader {

    private Integer id;
    private String batchId;
    private Long sizeBytes;
    private String sha256;
    private String path;
    private String fname;

    FileHeader() {}

    public FileHeader(final Integer id, final String batchId, final Long sizeBytes, final String sha256, final String path, final String fname) {
        this.id = id;
        this.batchId = batchId;
        this.sizeBytes = sizeBytes;
        this.sha256 = sha256;
        this.path = path;
        this.fname = fname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getSizeBytes() {
        return sizeBytes;
    }

    public void setSizeBytes(Long sizeBytes) {
        this.sizeBytes = sizeBytes;
    }

    public String getSha256() {
        return sha256;
    }

    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
