package co.redeye.core.scoping.dao;

import co.redeye.core.scoping.objects.FileMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by george on 25/09/15.
 */
public class FileMetadataDao {

    private static final Logger logger = LoggerFactory.getLogger(FileMetadataDao.class);

    private static Connection connection;

    public FileMetadataDao(final Connection connection) {
        this.connection = connection;
    }

    public void create(final FileMetadata fileMetadata) throws SQLException {
        String sql = "insert into file_metadata (file_id, key_id, value) values (?, ?, ?)";

        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, fileMetadata.getFileId());
        statement.setInt(2, fileMetadata.getKeyId());
        statement.setString(3, fileMetadata.getValue());

         if (statement.executeUpdate() == 0) {
            logger.debug("we should have got some rows created...");
        }
    }
}
